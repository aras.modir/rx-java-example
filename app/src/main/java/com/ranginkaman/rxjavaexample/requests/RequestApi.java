package com.ranginkaman.rxjavaexample.requests;

import com.ranginkaman.rxjavaexample.models.Comment;
import com.ranginkaman.rxjavaexample.models.Post;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RequestApi {

    @GET("posts")
    Observable<List<Post>> getPosts();

    @GET("posts/{id}/comments")
    Observable<List<Comment>> getComments(
            @Path("id") int id
    );


    @GET("todos/1")
    Flowable<ResponseBody> makeQuery();
}
