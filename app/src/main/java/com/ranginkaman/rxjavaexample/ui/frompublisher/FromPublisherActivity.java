package com.ranginkaman.rxjavaexample.ui.frompublisher;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.ranginkaman.rxjavaexample.R;

import java.io.IOException;

import okhttp3.ResponseBody;

public class FromPublisherActivity extends AppCompatActivity {

    private static final String TAG = "FromPublisherActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_from_publisher);

        FromPublisherViewModel viewModel = ViewModelProviders.of(this).get(FromPublisherViewModel.class);
        viewModel.makeQuery().observe(this, new Observer<ResponseBody>() {
            @Override
            public void onChanged(ResponseBody responseBody) {
                Log.d(TAG, "onChanged: this is a live data response!");
                try {
                    Log.d(TAG, "onChanged: " + responseBody.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
