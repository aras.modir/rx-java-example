package com.ranginkaman.rxjavaexample.ui.frompublisher;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import okhttp3.ResponseBody;

public class FromPublisherViewModel extends ViewModel {

    private Repository repository;

    public FromPublisherViewModel() {
        repository = Repository.getInstance();
    }

    public LiveData<ResponseBody> makeQuery() {
        return repository.makeReactiveQuery();
    }
}
